import React from 'react';
import {Planet, getSunObject, getPlanetObjects} from './planet.js';
import utils from './utils.js';

/**
 * Orbit Component.
 * Displays the orbit line of a Planet object.
 * 
 * @param {object} props is an object containing values passed from parent.
 */
const OrbitUi = (props) => {
  //Coverts the orbit ratio value passed as a prop into pixels, constrained by window height.
  const orbit = utils.orbitRadiusPixels(props.orbit, props.height);
  const thickness = utils.orbitThickness(props.orbit, Planet.orbitThickness);

  //Return statement for this React Component.
  // Assumes every instance of this component is wrapped by <svg/> tag.
  return (
    <circle
      cx={props.width * 0.6} cy='50%' r={orbit} fillOpacity={0}
      stroke={Planet.orbitColor} strokeWidth={thickness}
    />
  );
}

/**
 * Calculates paramters necessary to draw circle SVGs in PlanetUi Component.
 * 
 * @param {object} planet is of class Planet and is the planet to configure.
 * @param {number} width is the current width of the screen.
 * @param {number} height is the current height of the screen.
 * @param {number} time is the time in ms from Planet.initDate.
 */
const planetConfig = (planet, width, height, time) => {
  //Points to planet properties to pass on return.
  const stroke = Planet.outlineColor;
  const strokeWidth = Planet.outlineThickness;
  const fill = planet.color;
  
  //Calculates orbit radius and planet radius in pixels.
  const orbitR = utils.orbitRadiusPixels(planet.orbit, height);
  const planetR = utils.planetRadiusPixels(planet.diameter, height);

  //Calculates current x y position of planet.
  const cX = width * 0.6 + utils.orbitX(planet.initRad(), planet.rateRad(), orbitR, time);
  const cY = height * 0.5 + utils.orbitY(planet.initRad(), planet.rateRad(), orbitR, time);

  return {cX, cY, planetR, fill, stroke, strokeWidth};
}

/**
 * Planet Component.
 * Displays a planet for every Planet object.
 * 
 * @param {object} props is an object containing values passed from parent.
 */
const PlanetUi = (props) => {
  const handlers = props.handlers;
  const planet = props.planet;
  //Deconstructs return value from planetConfig().
  const {cX, cY, planetR, fill, stroke, strokeWidth}
    = planetConfig(planet, props.width, props.height, props.time);

  //Return statement for this React Component.
  // Assumes every instance of this component is wrapped by <svg/> tag.
  return (
      <circle
        onMouseEnter={e => handlers.onPlanetEnter(e, planet)}
        onMouseLeave={e => handlers.onPlanetLeave(e)}
        cx={cX} cy={cY} r={planetR} fill={fill}
        stroke={stroke} strokeWidth={strokeWidth}
      />
  );
}

/**
 * Sol Component.
 * Main component for displaying the solar system.
 * 
 * @param {object} props 
 */
const Sol = (props) => {
  const w = props.width;
  const h = props.height;
  
  //Return statement for this React Component.
  return (
    <svg width='100%' height='100%'>
      {/* Orbits */}
      {getPlanetObjects().map(({name, orbit}) => (
        <OrbitUi key={`${name}_orbit`} orbit={orbit} width={w} height={h} />
      ))}
      {/* Planets */}
      {getPlanetObjects().map(p => (
        <PlanetUi key={`${p.name}_planet`} handlers={props.handlers}
          planet={p} width={w} height={h} time={props.time}
        />
      ))}
      {/* Sun */}
      <PlanetUi key='sun_planet' handlers={props.handlers}
        planet={getSunObject()} width={w} height={h} time={props.time}
      />
    </svg>
  );
}

//Default export declaration
export default Sol;