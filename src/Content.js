import React from 'react';
import './Content.css';
import {TextUi, MenuItemUi} from './Labels.js';

/**
 * Side content component.
 * Displays content in the sidebar.
 * 
 * @param {object} props 
 */
const SideContentUi = (props) => {
  return (
    <div className='sideContent'>
      {props.text().map((p, i) => (
        <TextUi key={`${props.parentKey}_${i}`} string={p} width={props.width}/>
      ))}
    </div>
  );
}

/**
 * Menu component.
 * Displays menu items as a group.
 * 
 * @param {object} props 
 */
const MenuUi = (props) => {
  return (
    <div className='menu'>
      {props.items().map(item => 
        <MenuItemUi key={`menuitem_${item.label}`} handler={props.handler} item={item} width={props.width} />
      )}
    </div>
  );
}

export {SideContentUi, MenuUi};