import React from 'react';
import utils from './utils.js'
import './Labels.css';

/**
 * Determines CSS class to use depending on text index and text array length.
 * 
 * @param {number} i is the index of a line in a paragraph.
 * @param {number} length is the amount of lines in a paragraph.
 */
const textClass = (i, length) => {
  //Returns no class if paragraph size is one.
  if(i === 0 && length === 1) return '';

  //Returns the CSS class corresponding to the position of the line in the paragraph.
  if(i === 0) return 'top';
  else if(i === length - 1) return 'btm';
  else return 'mid';
}

/**
 * Configures PlanetLabelUi component for use.
 * @param {object} props 
 */
const planetConfig = (props) => {
  //Calculates orbit radius and planet radius in pixels.
  const planet = props.planet;
  const orbitR = utils.orbitRadiusPixels(planet.orbit, props.height);
  const pName = planet.name.charAt(0).toUpperCase() + planet.name.slice(1);

  // //Calculates current x y position of planet.
  // const cX = props.width * 0.6 + utils.orbitX(planet.initRad(), planet.rateRad(), orbitR, props.time);
  // const cY = props.height * 0.5 + utils.orbitY(planet.initRad(), planet.rateRad(), orbitR, props.time);

  const cX = props.width * 0.6;
  const cY = props.height * 0.5;

  return {cX, cY, pName};
}

/**
 * Planet Label UI component.
 * Displays and follows planets when they're hovered over with a cursor.
 * @param {*} props 
 */
const PlanetLabelUi = (props) => {
  const {cX, cY, pName} = planetConfig(props);
  const display = props.visibility ? 'initial' : 'none';
  
  return (
    <p style={{display: display, left: cX, bottom: cY}} className='label thin plabel'>{pName}</p>
  );
}

/**
 * Timer Component.
 * Displays a label with the current date, formatted in a time fashion.
 * @param {*} props 
 */
const TimerUi = (props) => {
  return(
    <p className='label thick timer'>{props.dateStr}</p>
  );
}

/**
 * Menu Item component.
 * Displays menu items in the Menu UI component.
 * @param {object} props 
 */
const MenuItemUi = (props) => {
  const item = props.item;
  const labelUpper = ' ' + item.label.charAt(0).toUpperCase() + item.label.slice(1);;

  //TODO: eliminate use of constants here
  //TODO: move this to parent?
  const display = props.width > 550 ? 'initial' : 'none';

  return (
    <button onClick={(e) => props.handler(e, item.onClick)} className='label thin menuItem'>
      <svg style={{verticalAlign: 'middle',}} width={16} height={16} viewBox="0 0 24 24">
        <path fill='#B1AE96' d={item.iconPath} />
      </svg>
      <span style={{display: display, verticalAlign: 'bottom',}}>{labelUpper}</span>
    </button>
  );
}

/**
 * Text Component.
 * Displays text as paragraphs.
 * @param {object} props 
 */
const TextUi = (props) => {
  //TODO: eliminate use of constants here
  //TODO: move this to parent?
  const maxWidth = Math.min(440, props.width - 80);
  const textWrapped = utils.wrapWords(maxWidth, props.string);
  
  return (
    <React.Fragment>
      {/* Text lines */}
      {textWrapped.map((line, i) => (
        <p key={i} className={`label thin ${textClass(i, textWrapped.length)}`}>{line}</p>
      ))}
    </React.Fragment>
  );
}

//Declares objects to export from file
export {TextUi, PlanetLabelUi, TimerUi, MenuItemUi};