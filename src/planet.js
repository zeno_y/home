/**
 * Planet class. Holds data on every major celestial body.
 * Exposes method for converting degrees to radians.
 */
class Planet {
  /**
   * Constructs object.
   * @param {string} name is the name of the celestial body.
   * @param {string} color is the color hex value of the celestial body. must start with '#'.
   * @param {number} diameter is the diameter ratio of the celestial body, compared to the largest body in the system.
   * @param {number} orbit is the orbit diameter ratio celestial body, compared to the furthest body from the sun.
   * @param {number} initA is the postion of the celestial body on Planet.initDate.
   * @param {number} periodDays the Earth-days for the celestial body to make one revolution around the sun.
   */
  constructor(name, color, diameter, orbit, initA, periodDays) {
    this.name = name;
    this.color = color;
    this.diameter = diameter;
    this.orbit = orbit;
    this.initA = initA;
    this.periodDays = periodDays;
  }

  /**
   * Returns the initial angle Planet.initA of the celestial body in radians.
   */
  initRad() {
    return 2 * Math.PI * this.initA / 360;
  }

  rateRad() {
    return this.periodDays > 0 ? 2 * Math.PI / this.periodDays : 0;
  }
}

/**
 * Static planet properties.
 */
//Describes initial date used for setting Planet.initA.
Planet.initDate = new Date(2019, 3, 1);
//Describes outline color used on a planet Component.
Planet.outlineColor = '#E5E3C3';
//Describes the outline thickness of a planet Component.
Planet.outlineThickness = 3;
//Describes the orbit color used on an orbit Component.
Planet.orbitColor = '#B1AE96';
//Describes the orbit line thickness used for an orbit Component.
Planet.orbitThickness = 1.2;

/**
 * Sun object extending Planet.
 * Uses only some of Planet's properties
 */
class Sun extends Planet {
  constructor() {
    super('sun', '#B1AE96', 0.5, 0, 0, 0);
  }
}

//Declares a new Sun object
function getSunObject() {
  const sun = getSunObject.sun || new Sun();
  return sun;
}

//Declares an array of Planet objects
function getPlanetObjects() {
  const planets = getPlanetObjects.planets || [
    new Planet('mercury', '#958E7E', 0.034, 0.012, 350, 87.9),
    new Planet('venus', '#CBBDAC', 0.084, 0.024, 31, 224.7),
    new Planet('earth', '#94A0AF', 0.089, 0.033, 292, 365.2),
    new Planet('mars', '#A48B8B', 0.048, 0.051, 185, 687.0),
    new Planet('jupiter', '#CFCFAF', 1, 0.173, 344, 4332.0),
    new Planet('saturn', '#DECDBC', 0.843, 0.319, 14, 10759.0),
    new Planet('uranus', '#9CB8A1', 0.358, 0.639, 122, 30688.0),
    new Planet('neptune', '#90A3AA', 0.346, 1, 76, 60182),
  ];

  return planets;
}

//Declares objects to export from file
export {Planet, Sun, getSunObject, getPlanetObjects};