/**
 * Determines the pixel length of a string.
 * TODO: pass font value.
 * @param {string} text is the text to measure.
 * @returns {number} is the length in pixels of the text as one line.
 */
function getTextWidth(text) {
  //Creates or reuses canvas element. Used to lay out text for measuring.
  let canvas = getTextWidth.canvas || (getTextWidth.canvas = document.createElement('canvas'));

  let ctx = canvas.getContext('2d'); //Get 2D context from canvas.
  ctx.font = '1.5em monospace'; //Sets the font of the context.
  let metrics = ctx.measureText(text); //measures text within previously configured context.

  return metrics.width;
}

/**
 * Splits a string into an array of words.
 * 
 * @param {string} text is the text to split into an array of words.
 * @returns {string[]} is an array of words.
 */
function splitWords(text) {
  return text.trim().split(' ');
}

/**
 * Stores constants and functions used throughout the app.
 */
const utils = {
  //Max elapsed time allowed before dayOffset is incremented.
  msAmount: 1000,
  //Interval of time to update elapseMs.
  msIncrement: 50,
  //Max diameter of planets in pixels.
  planetDiamMax: 70,
  //Ratio value used to change planets' diameter ratio.
  // 0 maximizes the difference while 1 makes all diameters equal.
  diametersRatio: 0.30,
  //Ratio value used to change planets' orbit ratio.
  // 0 maximizes the difference while 1 makes all the orbits equal.
  orbitsRatio: 0.16,

  /**
   * Converts planet diameter in planets ratio to radius in pixels.
   * 
   * @param {number} diameter is the diameter of a planet.
   * @param {number} screenheight is the current height of the window.
   * @returns {number} is the diameter converted to pixels.
   */
  planetRadiusPixels: (diameter, screenHeight) => {
    //TODO: remove constant here
    //TODO: not clear when diameter is converted to radius.
    const diameterMax = utils.planetDiamMax * screenHeight / 917;
    return diameterMax * (diameter + (1 - diameter) * utils.diametersRatio) / 2;
  },
  /**
   * Converts planet orbit diameter in orbits ratio to radius in pixels.
   * 
   * @param {number} orbit is the orbit (diameter) of a planet.
   * @param {number} screenheight is the current height of the window.
   * @returns {number} is the orbit (in radius) converted to pixels.
   */
  orbitRadiusPixels: (diameter, screenHeight) => {
    //TODO: not clear when diameter is converted to radius.
    const diameterMax = screenHeight - utils.planetDiamMax;
    return diameter === 0 ? 0 : diameterMax * (diameter + (1 - diameter) * utils.orbitsRatio) / 2;
  },
  /**
   * Adjusts orbit thickness depending on planet orbit.
   * 
   * @param {number} diameter is the orbit diameter of a planet.
   * @param {number} maxThickness is the max thickness for the outermost orbit to be.
   * returns {number} is the orbit thickness adjusted for orbit (outermost orbit is thickest).
   */
  orbitThickness: (diameter, maxThickness) => {
    const minThickness = maxThickness / 2;
    return minThickness + (diameter * minThickness);
    // return (diameter + (1 - diameter)) * maxThickness;
  },
  /**
   * Calculates planet orbit X position.
   * 
   * @param {number} initRad is the initial angle of the planet in radians.
   * @param {number} rateRad is the rate of change in angle (in radians).
   * @param {number} orbitR is the orbit radius of a planet.
   * @param {number} ms is the time elapsed since the initial date configured in Planet.initDate
   * @returns {number} is the x position of a planet at time ms
   */
  orbitX: (initRad, rateRad, orbitR, ms) => {
    return orbitR * Math.sin(initRad + rateRad * ms / 1000);
  },
  /**
   * Calculates planet orbit Y position.
   * 
   * @param {number} initRad is the initial angle of the planet in radians.
   * @param {number} rateRad is the rate of change in angle (in radians).
   * @param {number} orbitR is the orbit radius of a planet.
   * @param {number} ms is the time elapsed since the initial date configured in Planet.initDate
   * @returns {number} is the y position of a planet at time ms
   */
  orbitY: (initRad, rateRad, orbitR, ms) => {
    return orbitR * Math.cos(initRad + rateRad * ms / 1000);
  },
  /**
   * Converts a long text string into a paragraph that fits within a provided width.
   * Creates an array of lines, word by word, determining whether the line is shorter
   * than maxWidth for every word.
   * 
   * @param {number} maxWidth is the max width the text is allowed to be.
   * @param {string} text is the text string to wrap.
   * @returns {string[]} is the text string split into lines, wrapped.
   */
  wrapWords: (maxWidth, text) => {
    let textSplit = splitWords(text); //splits text string into words.
    let lineArray = []; //initializes array to return.

    let linePos = 0; //Keeps track of lineArray index.
    lineArray[linePos] = textSplit[0]; //Places the first word into the first array element.
    
    //Loops through every word, word by word.
    for(let i = 1; i < textSplit.length; i += 1) {
      //Temp line to store if conditions are met.
      const lineTemp = lineArray[linePos] + ' ' + textSplit[i];

      //Stores line with new word if new word doesn't exceed maxWidth
      if(getTextWidth(lineTemp) < maxWidth) lineArray[linePos] = lineTemp;
      //If condition fails, increment lineArray index and store word as new array element.
      else {
        linePos += 1;
        lineArray[linePos] = textSplit[i];
      }
    }

    return lineArray;
  },
};

//Default export declaration
export default utils;