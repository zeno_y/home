/**
 * TEMPORARY.
 * Retrieves home page text.
 */
function getHomeText() {
  const text = getHomeText.text || [
    'Welcome to my website.',
    // 'There\'s not much here right now. Come back in a minute or so, see if I\'ve made some updates.',
    'This page was developed in ReactJS. The future plan is to configure the celestial bodies for interactivity and set up a journal that stores entries using a backend.',
    'The solar system shown is not to scale. The planet positions in respect to Earth, however, are an approximation to reality. On the date shown below the positions are accurate to roughly one degree (+/-1°).',
  ];

  return text;
}

/**
 * Stores menu item info:
 * Label name, SVG icon path, on click event handler.
 */
class MenuItem {
  constructor(label, onClick, iconPath) {
    this.label = label;
    this.onClick = onClick;
    this.iconPath = iconPath;
  }
}

/**
 * Handles Home menu item click
 */
const onClickHome = () => {
  console.log('home clicked');
}

/**
 * Handles Journal menu item click
 */
const onClickJournal = () => {
  console.log('journal clicked');
}

/**
 * Handles Resume menu item click
 */
const onClickResume = () => {
  window.open('./resume.pdf');
}

/**
 * Handles Profile menu item click
 */
const onClickProfile = () => {
  window.open('https://www.linkedin.com/in/zeno-y-33915232');
}

/**
 * Handles Repo menu item click
 */
const onClickRepo = () => {
  window.open('https://www.bitbucket.org/zeno_y/');
}

/**
 * Retrieves menu items
 */
function getMenuItems() {
  const menuItems = getMenuItems.menuItems || [
    new MenuItem('home', onClickHome, 'M10 19v-5h4v5c0 .55.45 1 1 1h3c.55 0 1-.45 1-1v-7h1.7c.46 0 .68-.57.33-.87L12.67 3.6c-.38-.34-.96-.34-1.34 0l-8.36 7.53c-.34.3-.13.87.33.87H5v7c0 .55.45 1 1 1h3c.55 0 1-.45 1-1z'),
    // new MenuItem('journal', onClickJournal, 'M18 2H6c-1.1 0-2 .9-2 2v16c0 1.1.9 2 2 2h12c1.1 0 2-.9 2-2V4c0-1.1-.9-2-2-2zM6 4h5v8l-2.5-1.5L6 12V4z'),
    // new MenuItem('resume', onClickResume, 'M16.59 9H15V4c0-.55-.45-1-1-1h-4c-.55 0-1 .45-1 1v5H7.41c-.89 0-1.34 1.08-.71 1.71l4.59 4.59c.39.39 1.02.39 1.41 0l4.59-4.59c.63-.63.19-1.71-.7-1.71zM5 19c0 .55.45 1 1 1h12c.55 0 1-.45 1-1s-.45-1-1-1H6c-.55 0-1 .45-1 1z'),
    new MenuItem('resume', onClickResume, 'M18 2H6c-1.1 0-2 .9-2 2v16c0 1.1.9 2 2 2h12c1.1 0 2-.9 2-2V4c0-1.1-.9-2-2-2zM6 4h5v8l-2.5-1.5L6 12V4z'),
    new MenuItem('profile', onClickProfile, 'M12 2C6.48 2 2 6.48 2 12s4.48 10 10 10 10-4.48 10-10S17.52 2 12 2zm0 3c1.66 0 3 1.34 3 3s-1.34 3-3 3-3-1.34-3-3 1.34-3 3-3zm0 14.2c-2.5 0-4.71-1.28-6-3.22.03-1.99 4-3.08 6-3.08 1.99 0 5.97 1.09 6 3.08-1.29 1.94-3.5 3.22-6 3.22z'),
    new MenuItem('repo', onClickRepo, 'M8.7 15.9L4.8 12l3.9-3.9c.39-.39.39-1.01 0-1.4-.39-.39-1.01-.39-1.4 0l-4.59 4.59c-.39.39-.39 1.02 0 1.41l4.59 4.6c.39.39 1.01.39 1.4 0 .39-.39.39-1.01 0-1.4zm6.6 0l3.9-3.9-3.9-3.9c-.39-.39-.39-1.01 0-1.4.39-.39 1.01-.39 1.4 0l4.59 4.59c.39.39.39 1.02 0 1.41l-4.59 4.6c-.39.39-1.01.39-1.4 0-.39-.39-.39-1.01 0-1.4z'),
  ];

  return menuItems;
}

export {getHomeText, getMenuItems};