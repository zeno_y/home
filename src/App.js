import React from 'react';
import './App.css';
import Sol from './Sol.js';
import {PlanetLabelUi, TimerUi} from './Labels.js';
import utils from './utils.js';
import {getHomeText, getMenuItems} from './text.js';
import {SideContentUi, MenuUi} from './Content.js';

/**
 * Declares module for storing dayOffset value.
 */
let module = (function() {
  let dayOffset = 0;
  let pLabel = {};

  const getOffset = () => dayOffset;
  const setOffset = offset => dayOffset = offset;

  const getPLabel = () => pLabel;
  const setPLabel = planet => pLabel = planet;

  return {getOffset, setOffset, getPLabel, setPLabel};
}());

/**
 * 'use' arrow function that holds all necessary config for App.
 */
const useAppState = () => {
  //Declares States
  const [width, setWidth] = React.useState(0);
  const [height, setHeight] = React.useState(0);
  const [elapseMs, setElapseMs] = React.useState(0);
  const [showPlanetLabel, setShowPlanetLabel] = React.useState(false);

  //TODO: move to utils
  // const msAmount = 1000; //Max elapsed time allowed before dayOffset is incremented.
  // const msIncrement = 50; //Interval of time to update elapseMs.

  //Arrow function used to update window dimensions in Effects.
  const updateWindowDimens = () => {
    setWidth(window.innerWidth);
    setHeight(window.innerHeight);
  }

  //Arrow function that sets showPlanetLabel flag to true for mouse enter event on planet components.
  const onPlanetEnter = (e, planet) => {
    e.preventDefault();
    module.setPLabel(planet);
    setShowPlanetLabel(true);
    console.log('entered');
  }

  //Arrow function that sets showPlanetLabel flag to false for mouse leave event on planet components.
  const onPlanetLeave = (e) => {
    e.preventDefault();
    setShowPlanetLabel(false);
  }

  const onMenuClick = (e, onItemClick) => {
    e.preventDefault();
    onItemClick();
  }

  //Window resize side-effect.
  React.useEffect(() => {
    //Reliably updates window dimensions if width or height are zero.
    if(width === 0 || height === 0) updateWindowDimens();
    //Adds 'resize' event listener to the window.
    window.addEventListener('resize', updateWindowDimens);
    //On useEffect return, remove event listener to 'clean up'.
    return () => window.removeEventListener('resize', updateWindowDimens);
  });

  //Milliseconds elapsed side-effect.
  React.useEffect(() => {
    //Updates elapsed time only if elapsed time is less than msAmount.
    if(elapseMs < utils.msAmount) {
      // console.log(elapseMs);
      //Sets timeout function to update elapsed time in increments of msIncrement
      const timerId = setTimeout(() => setElapseMs(elapseMs + utils.msIncrement), utils.msIncrement);
      //On useEffect return, clears previously declared timeout.
      return () => clearTimeout(timerId);
    //Increments dayOffset by one when elapse time is greater than the defined max elapse time amount.
    } else {
      setElapseMs(0);
      module.setOffset(module.getOffset() + 1);
      // dayOffset += 1;
      // setOffset();
    }
  });

  //Define date object used to display date of current system position.
  let current = new Date();
  current.setDate(current.getDate() + module.getOffset()); 

  //Arrow function used to display date object in a readable manner.
  const dateStr = () => (
    `${current.getDate() < 10 ? '0': ''}` +
    `${current.getDate()}::` +
    `${current.getMonth() < 9 ? '0': ''}` +
    `${current.getMonth() + 1}::` +
    `${current.getFullYear()}`
  );

  const dayOffset = module.getOffset();
  const msAmount = utils.msAmount;
  const time = elapseMs + dayOffset * msAmount;
  const handlers = {onPlanetEnter, onPlanetLeave, onMenuClick};

  //Returns multiple variables and functions as one object.
  return {width, height, dateStr, time, showPlanetLabel, handlers};
}

/**
 * Main App Component.
 * Calculates window width/height on load and on window size change.
 * Runs a timer, calculates millisecond intevals on when UI should update and also keeps track of days passed.
 * Displays webapp text, timer, and the solar system.
 */
const App = () => {
  //Deconstruct return object from useAppState().
  const {width, height, dateStr, time, showPlanetLabel, handlers} = useAppState();

  //Return statement for this React Component.
  return (
    <div className='App'>
      {/* Sidebar */}
      <div className='content'>
        {/* Title */}
        <MenuUi key='menu_ui' handler={handlers.onMenuClick} items={getMenuItems} width={width} />
        {/* Text content */}
        <SideContentUi key='side_content' parentKey={'side_content'} text={getHomeText} width={width} />
      </div>
      {showPlanetLabel ? <PlanetLabelUi key={`planet_label`} visibility={showPlanetLabel}
          planet={module.getPLabel()} width={width} height={height} time={time}
        /> : ''}
      {/* Timer */}
      <TimerUi key='timer_ui' dateStr={dateStr()} />
      {/* Solar system */}
      <Sol key='sol_ui' handlers={handlers} width={width} height={height} time={time} />
    </div>
  );
}

//Default export declaration
export default App;
